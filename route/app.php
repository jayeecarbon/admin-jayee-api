<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 首页列表
Route::get('home', 'home.index/index');
Route::group(function () {
// 菜单模块列表路由
Route::get('commonconfig', 'home.CommonConfig/index');
// 用户常用功能配置
Route::post('commonconfig/deal', 'home.CommonConfig/deal');
// 资讯列表路由
Route::get('newsfeed', 'home.NewsFeed/index');
// 资讯详情路由
Route::get('newsfeedsee', 'home.NewsFeed/see');
// 历史通知列表路由
Route::get('notices', 'home.Notices/index');
// 历史通知详情路由
Route::get('noticessee', 'home.Notices/see');
// 通知发布
Route::post('notices/add', 'home.Notices/add');
// 通知编辑
Route::post('notices/edit', 'home.Notices/edit');
// 通知列表
Route::get('noticeslist', 'home.Notices/list');
// 通知详情
Route::get('noticesinfo', 'home.Notices/info');
// 删除消息
Route::post('noticesdel', 'home.Notices/del');
    Route::post('noticesupload', 'home.Notices/upload');
// 产品列表路由
Route::get('product', 'product.Product/index');
// 产品详情路由
Route::get('productsee', 'product.Product/see');
// 产品文件列表路由
Route::get('productfile', 'product.Product/file');
// 产品文件下载路由
Route::get('productdownload', 'product.Product/download');
// 新增产品路由
Route::post('product/add', 'product.Product/add');
// 编辑产品路由
Route::post('product/edit', 'product.Product/edit');
// 更新产品状态路由
Route::post('product/state', 'product.Product/state');
// 产品文件上传路由
Route::post('product/upload', 'product.Product/upload');

// 原材料列表路由
Route::get('productmaterial', 'product.ProductMaterial/index');
// 新增原材料路由
Route::post('productmaterial/add', 'product.ProductMaterial/add');
// 编辑原材料路由
Route::post('productmaterial/edit', 'product.ProductMaterial/edit');
// 删除原材料路由
Route::post('productmaterial/del', 'product.ProductMaterial/del');

// 产品核算列表路由
Route::get('productcalculate', 'product.ProductCalculate/index');
// 产品核算详情路由
Route::get('productcalculatesee', 'product.ProductCalculate/see');
// 生成核算报告路由
Route::get('productcalculatecreate', 'product.ProductCalculate/create');
// 新增产品核算路由
Route::post('productcalculate/add', 'product.ProductCalculate/add');
// 编辑产品核算路由
Route::post('productcalculate/edit', 'product.ProductCalculate/edit');
// 删除产品核算路由
Route::post('productcalculate/del', 'product.ProductCalculate/del');
// 更新产品核算状态路由
Route::post('productcalculate/state', 'product.ProductCalculate/state');

// 产品核算数据管理列表路由
Route::get('productdata', 'product.ProductData/index');
// 产品核算数据管理详情路由
Route::get('productdatasee', 'product.ProductData/see');
// 新增产品核算数据管理路由
Route::post('productdata/add', 'product.ProductData/add');
// 编辑产品核算数据管理路由
Route::post('productdata/edit', 'product.ProductData/edit');
// 删除产品核算数据管理路由
Route::post('productdata/del', 'product.ProductData/del');
// 选择产品/因子路由
Route::post('productdata/choice', 'product.ProductData/choice');

// 产品核算报告列表路由
Route::get('calculatereport', 'product.CalculateReport/index');
// 产品核算周期列表路由
Route::get('calculatereportweeks', 'product.CalculateReport/weeks');
// 产品核算报告详情路由
Route::get('calculatereportsee', 'product.CalculateReport/see');
// 产品核算报告下载路由
Route::get('calculatereportdownload', 'product.CalculateReport/download');
// 新增产品核算报告路由
Route::post('calculatereport/add', 'product.CalculateReport/add');
// 编辑产品核算报告路由
Route::post('calculatereport/edit', 'product.CalculateReport/edit');
// 删除产品核算报告路由
Route::post('calculatereport/del', 'product.CalculateReport/del');

// 数据审批列表路由
Route::get('approval', 'product.Approval/index');
// 数据审批详情路由
Route::get('approvalsee', 'product.Approval/see');
// 更新数据审批状态路由
Route::post('approval/state', 'product.Approval/state');

// 排放因子列表路由
Route::get('factor', 'product.Factor/index');
// 排放因子详情路由
Route::get('factorsee', 'product.Factor/see');

// 用户列表路由
Route::get('user', 'system.User/index');
// 用户详情路由
Route::get('usersee', 'system.User/see');
// 新增用户路由
Route::post('user/add', 'system.User/add');
// 用户注册路由
Route::post('user/register', 'system.User/register');
// 更新用户状态路由
Route::post('user/state', 'system.User/state');
// 更新用户角色路由
Route::post('user/role', 'system.User/role');
// 更新用户组织路由
Route::post('user/group', 'system.User/group');

// 碳价管理列表路由
Route::get('carbonprice', 'admin.CarbonPrice/index');
// 碳价管理导出路由
Route::get('carbonpriceexport', 'admin.CarbonPrice/export');
// 碳价管理导入路由
Route::post('carbonpriceimport', 'admin.CarbonPrice/import');
// 碳价管理删除路由
Route::post('carbonprice/del', 'admin.CarbonPrice/del');

// 数据字典列表路由
Route::get('dictionary', 'admin.Dictionary/index');
// 数据字典数组路由
Route::get('dictionarysee', 'admin.Dictionary/see');
// 数据字典新增路由
Route::post('dictionary/add', 'admin.Dictionary/add');
// 数据字典修改路由
Route::post('dictionary/edit', 'admin.Dictionary/edit');
// 数据字典删除路由
Route::post('dictionary/del', 'admin.Dictionary/del');

// 单位管理列表路由
Route::get('unit', 'admin.Unit/index');
// 单位管理详情路由
Route::get('unitfind', 'admin.Unit/find');
// 单位管理数组路由
Route::get('unitsee', 'admin.Unit/see');
// 单位管理新增路由
Route::post('unit/add', 'admin.Unit/add');
// 单位管理修改路由
Route::post('unit/edit', 'admin.Unit/edit');
// 单位管理删除路由
Route::post('unit/del', 'admin.Unit/del');

// 操作日志列表路由
Route::get('operation', 'system.Operation/index');
// 操作日志列导出路由
Route::get('operationexport', 'system.Operation/export');
// 文件列表路由
Route::get('file', 'system.File/index');
// 文件下载路由
Route::get('filedownload', 'system.File/download');
// 文件上传路由
Route::post('file/upload', 'system.File/upload');
// 组织列表路由
Route::get('organization', 'system.Organization/index');
// 组织新增路由
Route::post('organization/add', 'system.Organization/add');
// 组织修改路由
Route::post('organization/edit', 'system.Organization/edit');
// 组织删除路由
Route::post('organization/del', 'system.Organization/del');
// 组织详情路由
Route::get('organizationsee', 'system.Organization/see');
// 组织回显路由
Route::get('organizationfind', 'system.Organization/find');
// 审批设置列表路由
Route::get('sysetapproval', 'system.Approval/index');
// 审批回显路由
Route::get('sysetapprovalsee', 'system.Approval/see');
// 组织修改路由
Route::post('sysetapproval/edit', 'system.Approval/edit');

// 产品碳足迹碳标签管理列表
Route::get('productcarbonlabel', 'product.ProductCarbonLabel/index');
// 产品碳足迹碳标签 获取新增表单接口
Route::get('productcarbonaddform', 'product.ProductCarbonLabel/addform');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonadd', 'product.ProductCarbonLabel/add');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonedit', 'product.ProductCarbonLabel/edit');
// 产品碳足迹碳标签 删除接口
Route::post('productcarbondel', 'product.ProductCarbonLabel/del');

// 碳数据产品排放分析列表
Route::get('productemission', 'data.ProductEmission/index');
// 碳数据产品排放分析产品详情
Route::get('productemissioninfo', 'data.ProductEmission/info');
// 碳数据产品排放分析产品加入对比
Route::post('productemissionadd', 'data.ProductEmission/add');
// 碳数据产品排放分析产品对比列表
Route::get('productemissionlist', 'data.ProductEmission/list');
// 碳数据产品排放分析产品对比删除
Route::post('productemissiondel', 'data.ProductEmission/del');
// 碳数据产品排放分析产品对比
Route::post('productemissioncontrast', 'data.ProductEmission/contrast');

})->middleware(\app\middleware\CheckToken::class);

