<?php
namespace app\model\system;

use think\facade\Db;

/**
 * ApprovalModel
 */
class ApprovalModel extends Db {

    /**
     * 是否开启-开启
     */
    CONST IS_ON_YES = 1;

    /**
     * 是否开启-关闭
     */
    CONST IS_ON_NO = 2;

    /**
     * 审批节点-工厂审批
     */
    CONST APPROVAL_NODE_FACTORY = 1;

    /**
     * 审批节点-分公司审批
     */
    CONST APPROVAL_NODE_BRANCH = 2;

    /**
     * 审批节点-母公司审批
     */
    CONST APPROVAL_NODE_PARENT = 3;

    /**
     * 审批节点类型
     */
    CONST APPROVAL_NODE_TYPE = [
        ['id'=>self::APPROVAL_NODE_FACTORY, 'name'=>'工厂审批'],
        ['id'=>self::APPROVAL_NODE_BRANCH, 'name'=>'分公司审批'],
        ['id'=>self::APPROVAL_NODE_PARENT, 'name'=>'母公司审批'],
    ];

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getApprovals 查询业务流程
     * 
	 * @return $list
     */
    public static function getApprovals() {
        $list = Db::table('jy_approval ja')
            ->field('ja.id, ja.name, ja.factory, ja.branch_company, ja.parent_company')
            ->order('ja.sort', 'asc')
            ->select();

        return $list;
    }

    /**
     * getApproval 查询业务流程
     * 
     * @param $id
	 * @return $list
     */
    public static function getApproval($id) {
        $list = Db::table('jy_approval ja')
            ->field('ja.id, ja.name, ja.factory, ja.branch_company, ja.parent_company')
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * editApproval 编辑审批
     * 
     * @param $data
	 * @return $list
     */
    public static function editApproval($data) {
        $edit = Db::table('jy_approval')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }
 
}