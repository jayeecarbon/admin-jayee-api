<?php
declare (strict_types = 1);

namespace app\model\user;

use think\facade\Db;

/**
 * UserModel
 */
class UserModel extends Db {

    /**
     * 是否启用-已启用
     */
    CONST STATE_TYPE_YES = 1;

    /**
     * 是否启用-未启用
     */
    CONST STATE_TYPE_NO = 2;

    /**
     * findUser 通过用户名查找用户
     * 
     * @param $username
	 * @return $list
     */
    public static function findUser($username) {

        $list = Db::table('jy_mange_user jcu')
            ->where('jcu.username', $username)
            ->find();

        return $list;
    }

    /**
     * updateUser 更新用户
     * 
     * @param $data
	 * @return $edit
     */
    public static function updateUser($data) {
        $edit = Db::table('jy_mange_user')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }
}
