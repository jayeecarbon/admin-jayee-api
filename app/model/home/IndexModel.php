<?php
declare (strict_types = 1);

namespace app\model\home;

use think\facade\Db;

/**
 * NoticesModel
 */
class IndexModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getConfigs 获取常用功能列表
     * 
     * @author wuyinghua
     * @param $num
     * @param $user_id
	 * @return $list
     */
    public static function getConfigs($num, $user_id) {

        $list = Db::table('jy_common_config jcc')
            ->field('jcc.id, jcc.menu_id, jm.id m_id, jm.title, jm.pid, jm.path')
            ->leftJoin('jy_menu jm', 'jm.id = jcc.menu_id')
            ->where('jcc.user_id', '=', $user_id)
            ->limit($num)
            ->order('m_id', 'asc')
            ->select();

        return $list;
    }

    /**
     * getMenus 获取菜单列表
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getMenus($id) {
        $list = Db::table('jy_menu')
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getNews 获取资讯列表
     * 
     * @author wuyinghua
     * @param $num
	 * @return $list
     */
    public static function getNews($num) {
        $list = Db::table('jy_notices')
            ->field('id, title, content, create_time')
            ->where('is_del', '=', '0')
            ->where('type', '=', NoticesModel::TYPE_NEWS)
            ->limit($num)
            ->order('create_time', 'desc')
            ->select();

        return $list;
    }

    /**
     * getNotices 获取通知列表
     * 
     * @author wuyinghua
     * @param $num
	 * @return $list
     */
    public static function getNotices($num) {
        $list = Db::table('jy_notices')
            ->field('id, title, create_time')
            ->where('is_del', '=', '0')
            ->where('type', '=', NoticesModel::TYPE_NOTICE)
            ->limit($num)
            ->order('create_time', 'desc')
            ->select();

        return $list;
    }
}
