<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
Route::post('login', 'user.User/login');
//退出
Route::post('loginout', 'user.User/loginout');
// 碳因子库获取详情



Route::group(function () {
    // 碳因子库管理列表
    Route::get('factorlist', 'factor.Factor/index');
// 碳因子库获取详情
    Route::get('factorfind', 'factor.Factor/find');
// 碳因子库 新增接口
    Route::post('factoradd', 'factor.Factor/add');
// 碳因子库编辑接口
    Route::post('factoredit', 'factor.Factor/edit');
// 碳因子库 启用禁止接口
    Route::post('factorchange', 'factor.Factor/changStatus');

// 碳因子库导出路由
    Route::get('factorexport', 'factor.Factor/export');
// 碳因子库导入路由
    Route::post('factorimport', 'factor.Factor/import');
//认证管理
//认证列表
Route::get('attestationlist', 'attestation.Attestation/index');
//添加认证
Route::post('attestationadd', 'attestation.Attestation/add');
//认证详情
Route::get('attestationinfo', 'attestation.Attestation/find');
//认证删除
Route::post('attestationdel', 'attestation.Attestation/del');
//认证编辑
Route::post('attestationedit', 'attestation.Attestation/edit');
//上传图片
Route::post('attestationupload', 'attestation.Attestation/upload');
//上传文件
Route::post('attestationuploadform', 'attestation.Attestation/uploadform');
//获取侧边栏
Route::get('leftmenu', 'attestation.Attestation/leftmenu');
//添加认证表单
Route::post('addform', 'attestation.Attestation/addform');
//编辑认证表单
Route::post('editform', 'attestation.Attestation/editform');
//快照因子详情
Route::get('shot_find', 'attestation.Attestation/shotFind');
//撤回
Route::post('attestationback', 'attestation.Attestation/back');
//提交认证
Route::post('submitattestation', 'attestation.Attestation/submitattestation');
//统计认证申请
Route::get('getcount', 'attestation.Attestation/getcount');
//删除认证表单
Route::post('delform', 'attestation.Attestation/delform');
//获取城市列表
Route::get('getcity', 'attestation.Attestation/getcity');
//通过审核/驳回
Route::post('pass', 'attestation.Attestation/Pass');
//数据正确/数据错误
Route::post('right', 'attestation.Attestation/Right');
//确认付款/取消
Route::post('pay', 'attestation.Attestation/Pay');
//下载报告
Route::get('getreport', 'attestation.Attestation/download');
//下载证书
Route::get('getcertificate', 'attestation.Attestation/download');
//批量对比
Route::post('batchcompar', 'attestation.Attestation/batchCompar');
//产品核算列表
Route::get('getproductcalculate', 'attestation.Attestation/productList');
//订单列表
Route::get('orderlist', 'attestation.Attestation/orderList');
//组织列表
Route::get('organizationlist', 'attestation.Attestation/organizationList');

Route::get('unitsee', 'attestation.Attestation/see');
//产品核算详情
Route::get('productcalculatesee', 'attestation.Attestation/seeinfo');

Route::post('loginout', 'user.User/loginout');
Route::get('factor', 'attestation.Attestation/factor');

})->middleware(\app\middleware\CheckToken::class);
