<?php
namespace app\api\controller\factor;

use app\BaseController;
use app\model\admin\FactorModel;
use app\model\system\OperationModel;
use app\validate\FactorValidate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use think\Request;


/**
 * Factor
 */
class Factor extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 因子列表
     *
     * @return void
     */
    public function index(Request $request) {

        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '10';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '1';
        $data_redis = $request->middleware('data_redis');
        $main_organization_id = $data_redis['main_organization_id'];
        $filters = [
            'title' => isset($_GET['title']) ? $_GET['title'] : '',
            'factor_id' => isset($_GET['factor_id']) ? $_GET['factor_id'] : '',
            'mechanism' => isset($_GET['mechanism']) ? $_GET['mechanism'] : '',
            'start' => isset($_GET['start']) ? $_GET['start'] : '',
            'end'=>isset($_GET['start']) ? $_GET['start'] : '',
            'country'=>isset($_GET['country']) ? $_GET['country'] : '',
            'factor_source' => isset($_GET['factor_source']) ? $_GET['factor_source'] : '',
            'status' => isset($_GET['status']) ? $_GET['status'] : 0,
            'grade' => isset($_GET['grade']) ? $_GET['grade'] : 0,
            'organization_id'=>$main_organization_id
        ];
        $db = new FactorModel();
        $list = $db->getFactorList($page_size,$page_index,$filters)->toArray();

        $grade=[
            ['option'=>0,'value'=>'全部'],
            ['option'=>1,'value'=>'1级'],
            ['option'=>2,'value'=>'2级'],
            ['option'=>3,'value'=>'3级']
        ];
        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['type']='detail';
        $data['data']['grade'] =$grade;


        return json($data);
    }

    /**
     * export 导出
     *
     * @return void
     */
    public function export(Request $request) {

        $data_redis = $request->middleware('data_redis');
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];

        $header = ["A1"=>"因子名称","B1" => "源语言","C1" => "名称（源语言）","D1" => "规格/型号","E1" => "场景描述", "F1" => "发布机构",
            "G1" => "发布机构简称", "H1" => "因子评级","I1" => "发布年份","J1" => "发布国家/组织","K1" => "发布地区","L1" => "来源文件名称",
            "M1" => "二氧化碳当量因子数值", "N1" => "二氧化碳当量分子单位","O1" => "二氧化碳当量分母单位","P1"=>"碳因子id",
            "Q1" => "编号","R1" => "创建时间"];
        $db = new FactorModel();
        $list = $db->getFactorAll($data_redis['main_organization_id'])->toArray();


        $arr = [];
        foreach ($list as $key => $value){
            $arr[$key] = array_values($value);
        }
        $count = count($arr);

        if($count>50000){
            return json(['code'=>404,'message'=>'数据量太大，请联系管理员导出！']);
        }
        $type= true;
        $fileName = '碳因子库导出' . time();


        // 添加操作日志

        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '碳因子管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '碳因子库导出：' .$fileName.'.xlsx';

        OperationModel::addOperation($data_log);
        self::getExport($header,$type,array_values($arr),rawurlencode($fileName));

    }

    /**
     * getExport
     *
     * @return void
     */
    public static function getExport($header = [], $type =false, $data = [], $fileName = "碳因子管理") {
        $preadsheet = new Spreadsheet();
        $sheet = $preadsheet->getActiveSheet();

        foreach ($header as $k => $v) {
            $sheet->setCellValue($k, $v);
        }

        $sheet->fromArray($data, null, "A2");
        $sheet->getDefaultColumnDimension()->setWidth(12);

        if ($type) {
            header("Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            $type = "Xlsx";
            $suffix = "xlsx";
        } else {
            header("Content-Type:application/vnd.ms-excel");
            $type = "Xls";
            $suffix = "xls";
        }

        ob_end_clean();
        header("Content-Disposition:attachment;filename=$fileName.$suffix");
        header("Cache-Control:max-age=0");

        $writer = IOFactory::createWriter($preadsheet, $type);
        $writer->save('php://output');
        die();


    }

    /**
     * import 导入
     *
     * @return void
     */
    public function import(Request $request) {
        $db = new FactorModel();
        // $del = $db->delCarbonPrice();
        $data_redis = $request->middleware('data_redis');

        $file[]= $request->file('file1');
        try {
            validate(['file' => 'filesize:51200|fileExt:xls,xlsx'])
                ->check($file);

            $savename =\think\facade\Filesystem::disk('public')->putFile( 'file', $file[0]);
            $fileExtendName = substr(strrchr($savename, '.'), 1);
            if(!in_array(strtolower($fileExtendName),['xls','xlsx'])){
                return json(['code'=>404,'message'=>'数据格式为空！']);
            };

            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Xls');
            }

            $objReader->setReadDataOnly(TRUE);

            $objPHPExcel = $objReader->load('storage/'.$savename);
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;
            if ($lines <= 0) {
                return json(['code'=>404,'message'=>'数据不能为空！']);
            }
            if($lines>20000){
                return json(['code'=>404,'message'=>'导入数据量太大，请分批次导入！！']);
            }

            for ($j = 2; $j <= $highestRow; $j++) {
                $data[$j - 2] = [
                    'title' => $objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue(),
                    'language' => $objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue(),
                    'title_language' => $objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue(),
                    'model' => $objPHPExcel->getActiveSheet()->getCell("D" . $j)->getValue(),
                    'describtion' => $objPHPExcel->getActiveSheet()->getCell("E" . $j)->getValue(),
                    'mechanism' => $objPHPExcel->getActiveSheet()->getCell("F" . $j)->getValue(),
                    'mechanism_short' => $objPHPExcel->getActiveSheet()->getCell("G" . $j)->getValue(),
                    'grade' => $objPHPExcel->getActiveSheet()->getCell("H" . $j)->getValue(),
                    'year' => (int)$objPHPExcel->getActiveSheet()->getCell("I" . $j)->getValue(),
                    'country' => $objPHPExcel->getActiveSheet()->getCell("J" . $j)->getValue(),
                    'region' => $objPHPExcel->getActiveSheet()->getCell("K" . $j)->getValue(),
                    'file_name' => $objPHPExcel->getActiveSheet()->getCell("L" . $j)->getValue(),
                    'factor_value' => $objPHPExcel->getActiveSheet()->getCell("M" . $j)->getValue(),
                    'molecule' => $objPHPExcel->getActiveSheet()->getCell("N" . $j)->getValue(),
                    'denominator' => $objPHPExcel->getActiveSheet()->getCell("O" . $j)->getValue(),
                    'factor_source'=>1,
                    'organization_id'=>$data_redis['main_organization_id'],
                    'factor_id'=>mt_rand(10,99).mt_rand(1,9).mt_rand(1,9).mt_rand(10,99).mt_rand(10,99).mt_rand(1,9),
                    'create_time' => date('Y-m-d H:i:s',time()),
                    'modify_by' => $data_redis['userid']
                ];
            }

            $add = $db->addFactor($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "上传成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "上传失败";
            }

            return json($datasmg);
        } catch (\think\exception\ValidateException $e) {

            return $e->getMessage();
        }
    }

    /**
     * add 添加因子库
     *
     * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data_redis = $request->middleware('data_redis');
            $data['title'] = $params_payload_arr['title'];
            $data['model'] = $params_payload_arr['model'];
            $data['grade'] =  $params_payload_arr['grade'];
            $data['uncertainty'] = $params_payload_arr['uncertainty'];
            $data['describtion'] = $params_payload_arr['describtion'];
            $data['factor_value'] = $params_payload_arr['factor_value'];
            $data['molecule'] = $params_payload_arr['molecule'];
            $data['denominator'] =  $params_payload_arr['denominator'];
            $data['mechanism'] =  $params_payload_arr['mechanism'];
            $data['year'] = $params_payload_arr['year'];
            $data['country'] = $params_payload_arr['country'];
            $data['region'] =  $params_payload_arr['region'];
            $data['file_name'] = $params_payload_arr['file_name'];
            $data['factor_source'] = 2;
            $data['organization_id'] = $data_redis['main_organization_id'];

            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['factor_id'] =mt_rand(10,99).mt_rand(1,9).mt_rand(1,9).mt_rand(10,99).mt_rand(10,99).mt_rand(1,9);


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '碳因子管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建碳因子：' . $data['title'];

            OperationModel::addOperation($data_log);
            $insert_data=[];
            $insert_data[]=$data;
            $add = FactorModel::addFactor($insert_data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * edit 修改因子库
     *
     * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['title'] = $params_payload_arr['title'];
            $data['model'] = $params_payload_arr['model'];
            $data['grade'] =  $params_payload_arr['grade'];
            $data['uncertainty'] = $params_payload_arr['uncertainty'];
            $data['describtion'] = $params_payload_arr['describtion'];
            $data['factor_value'] = $params_payload_arr['factor_value'];
            $data['molecule'] = $params_payload_arr['molecule'];
            $data['denominator'] =  $params_payload_arr['denominator'];
            $data['mechanism'] =  $params_payload_arr['mechanism'];
            $data['year'] = $params_payload_arr['year'];
            $data['country'] = $params_payload_arr['country'];
            $data['region'] =  $params_payload_arr['region'];
            $data['file_name'] = $params_payload_arr['file_name'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '碳因子库修改';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑碳因子库：' . $data['title'];

            OperationModel::addOperation($data_log);
            $edit = FactorModel::updateFactor($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * del 启用/禁用
     *
     * @return void
     */
    public function changStatus(Request $request) {
        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }
            $status = isset($params_payload_arr['status']) ? $params_payload_arr['status'] : '';
            if ($status == '' || $status == null || $status == 0) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }
            $data['id'] = $id;
            $data['status'] = $params_payload_arr['status'];
            $unit_info = FactorModel::getInfoById($id);


            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data['user_id'] = $data_redis['userid'];
            $data_log['module'] = '碳因子库管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '启用/禁用：' . $unit_info['title'];

            OperationModel::addOperation($data_log);
            $del = FactorModel::changeStatus($data);

            if($del){
                $datasmg['code'] = 200;
                $datasmg['message'] = "操作成功";
            }else{
                $datasmg['code'] = 404;
                $datasmg['message'] = "操作失败";
            }

            return json($datasmg);
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }
    }

    /**
     * find 查看碳因子库详情
     *
     * @return void
     */
    public function find() {
        $id = $_GET['id'];
        $list = FactorModel::getInfoById($id);
        $grade=[
            ['option'=>0,'value'=>'全部'],
            ['option'=>1,'value'=>'1级'],
            ['option'=>2,'value'=>'2级'],
            ['option'=>3,'value'=>'3级']
        ];
        $data['code'] = 200;
        $data['data'] = $list;
        $data['grade']=$grade;

        return json($data);
    }
    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
             validate(FactorValidate::class)->check($data);

            return  true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }
}