<?php
namespace app\controller\system;

use app\BaseController;
use think\exception\ValidateException;
use app\model\system\OperationModel;
use app\model\system\FileModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * File
 */
class File extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 文件列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        $module_list = FileModel::getModules()->toArray();

        $modules = [];
        foreach ($module_list as $key => $value) {
            $modules[$key] = $value['title'];
        }

        // 过滤条件：操作模块、操作人、操作开始时间、操作结束时间
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_module' => isset($_GET['filterModule']) ? $_GET['filterModule'] : '',
            'filter_time_start' => isset($_GET['filteTimeStart']) ? $_GET['filteTimeStart'] : '',
            'filter_time_end' => isset($_GET['filteTimeEnd']) ? $_GET['filteTimeEnd'] : '',
            'modules' => $modules,
            'main_organization_id' => $main_organization_id
        ];

        $list = FileModel::getFiles($page_size, $page_index, $filters)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['modules'] = $module_list;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * upload 上传文件
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function download() {
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        $data['download_type'] = isset($_GET['download_type']) ? $_GET['download_type'] : '';
        $list = FileModel::getFile($data['id']);

         if ($data['download_type'] == FileModel::FACTOR_TEMPLATE) {
            $filePath = './static/碳排放因子导入模板.xlsx';
            $fileName = '碳排放因子导入模板.xlsx';

            // 认证服务的文件
        }  elseif ($data['download_type'] == FileModel::CERTIFICATION_TYPE) {
            if ($list == NULL) {
                header('Content-Disposition: attachment;productfootnofile=productfootnofile');
                return json(['code'=>201, 'message'=>"文件不存在"]);
            }
            $filePath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/admin_cooperate_uploads' . $list['file_path'];
            $fileName = $list['source_name'];

            // 资讯的文件
        } elseif ($data['download_type'] == FileModel::NOTICE_TYPE) {
            if ($list == NULL) {
                header('Content-Disposition: attachment;productfootnofile=productfootnofile');
                return json(['code'=>201, 'message'=>"文件不存在"]);
            }
            $filePath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/admin_cooperate_uploads' . $list['file_path'];
            $fileName = $list['source_name'];

            // 产品碳足迹的文件下载
        }

        // 文件不存在，header返回前端标记productfootnofile
        if(!is_file($filePath)){
            header('Content-Disposition: attachment;productfootnofile=productfootnofile');
            return json(['code'=>201, 'message'=>"文件不存在"]);
        }


        return download($filePath, rawurlencode($fileName))->expire(300);
    }

    public function upload() {
        $data_redis = $this->request->middleware('data_redis');

        $file = $this->request->file();
        $ip = 'http://www.jayeecloud.com:81';
        try {
            // 验证文件大小格式
            validate(['file' => 'fileSize:52428800|fileExt:png,jpg'])
                ->check($file);

            $savename = \think\facade\Filesystem::disk('admin_cooperate_uploads')->putFile('admin_cooperate_uploads', $file['file']);
            // halt($savename);
            $url = str_replace('\\', '/', $savename);
            $uuid= guid();
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['file_path'] = str_replace('admin_cooperate_uploads', '', $url);
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['main_organization_id'] = $data_redis['main_organization_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['module'] = '消息管理';
            $data['id'] = $uuid;
            $add_id = FileModel::addFile($data);

            if ($add_id) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $uuid;
                $datasmg['name'] = $data['source_name'];
                $datasmg['file_path'] = $ip . $data['file_path'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        } catch (ValidateException $e) {
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    /**
     * download 文件下载
     * 
     * @author wuyinghua
	 * @return void
     */

    /**
     * exportExcel 导出Excel
     * 
     * @author wuyinghua
	 * @return void
     */
    public static function exportExcel($expTitle, $expCellName, $expTableData, $setWidth = []) {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setTitle($expTitle . date('Ymd'));
        $cellNum = count($expCellName);
        $len = count($expTableData);
    
        // 设置表头字段
        foreach ($expCellName as $k => $v) {
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($k + 1, 1, $v);
        }
    
        // 添加数据
        foreach ($expTableData as $k => $v) {
            for ($i = 0; $i < $cellNum; $i++) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($i + 1, $k + 2, $expTableData[$k][$expCellName[$i]]);
            }
        }
    
        // 添加所有边框/居中
        $styleArrayBody = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '333333'],
                ],
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
        ];
        $total_rows = $len + 1;		// 表格总行数要加上表头一栏
        $spreadsheet->getActiveSheet()->getStyle('A1:'.end($setWidth) . $total_rows)->applyFromArray($styleArrayBody);
    
        // 设置列宽
        if ($setWidth) {
            foreach ($setWidth as $k => $v) {
                $spreadsheet->getActiveSheet()->getColumnDimension($v)->setAutoSize(true);
                // $spreadsheet->getActiveSheet()->getColumnDimension($v)->setWidth(30);
                // $sheet->getColumnDimension('A')->setWidth(30);
            }
        } else {
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
        }
    
        $filename = $expTitle . date('Ymd') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
    
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        die();
    }

    /**
     * importExcel
     * 
     * @author wuyinghua
     * @return array|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public static function importExcel($filename) {
        $file[] = $filename;
        try {
            // 验证文件大小，名称等是否正确
            validate(['file' => 'filesize:51200|fileExt:xls,xlsx'])->check($file);
            // 把上传的excel文件下载到本地一份
            $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);
            // 读取本地的excel文件
            $spreadsheet = IOFactory::load('storage/' . $savename);
            $sheet = $spreadsheet->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            Coordinate::columnIndexFromString($highestColumn);
            // 去除表头后的总行数
            $lines = $highestRow - 1;
            if ($lines <= 0) {
                return "数据为空数组";
            }
            // 直接取出excel中的数据
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            // 删除第一个元素（表头）
            array_shift($sheetData);
            // 删除本地下载的文件
            unlink('storage/' . $savename);

            return $sheetData;
        } catch (ValidateException $e) {
            return $e->getMessage();
        }
    }
}