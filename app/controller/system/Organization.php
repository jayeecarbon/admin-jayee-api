<?php
namespace app\controller\system;

use app\BaseController;
use app\validate\OrganizationValidate;
use app\validate\OrganizationParentValidate;
use think\exception\ValidateException;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;

/**
 * Organization 组织管理
 */
class Organization extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 组织列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：组织
        $filters = [
            'main_organization_id' => $main_organization_id
        ];

        $list = OrganizationModel::getOrganizations($page_size, $page_index, $filters)->toArray();
        $list_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();

        // 上级组织不包括第三级 只显示所属母公司
        $superior_organization = [];
        foreach ($list_all as $key => $value) {
            $organization = OrganizationModel::getOrganization($value['pid']);
            if ($value['pid'] == 0 || $organization['pid'] == 0) {
                $superior_organization[$key]['id'] = $value['id'];
                $superior_organization[$key]['name'] = $value['name'];
            }
        }

        foreach ($list['data'] as $key => $value) {
            $province = OrganizationModel::getCity($value['province']);
            if ($province) {
                $list['data'][$key]['province'] = $province['name'];
            }

            $city = OrganizationModel::getCity($value['city']);
            if ($city) {
                $list['data'][$key]['city'] = $city['name'];
            }

            $area = OrganizationModel::getCity($value['area']);
            if ($area) {
                $list['data'][$key]['area'] = $area['name'];
            }

            $p_organization = OrganizationModel::getOrganization($value['pid']);
            if ($p_organization) {
                $list['data'][$key]['p_organization'] = $p_organization['name'];
            }
        }

        $citys = OrganizationModel::getCitys();
        $industries = OrganizationModel::getIndustries();
        // 获取三层父子集
        $city_tree = $this->getTree($citys, 0, 3);

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['superior_organization'] = array_values($superior_organization);
        $data['data']['city_tree'] = $city_tree;
        $data['data']['industries'] = $industries;
        $data['data']['organization_type'] = OrganizationModel::ORGANIZATION_TYPE_SELECT_MAP;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * add 组织添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');
            $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['name'] = $_POST['name'];
            $data['type'] = $_POST['type'];
            $data['pid'] = $_POST['pid'];
            $data['main_organization_id'] = $main_organization_id;
            $data['province'] = $_POST['province'];
            $data['city'] = $_POST['city'];
            $data['area'] = empty($_POST['area']) ? NULL : $_POST['area'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data['create_by'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '系统设置';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '添加组织：' . $data['name'];

            $add = OrganizationModel::addOrganization($data);
            OperationModel::addOperation($data_log);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 组织编辑
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['industry_id'] = empty($_POST['industry_id']) ? NULL : $_POST['industry_id'];
            $data['description'] = empty($_POST['description']) ? NULL : $_POST['description'];
            $data['contact_person'] = empty($_POST['contact_person']) ? NULL : $_POST['contact_person'];
            $data['telephone'] = empty($_POST['telephone']) ? NULL : $_POST['telephone'];
            $data['pid'] = empty($_POST['pid']) ? 0: $_POST['pid'];
            $data['province'] = $_POST['province'];
            $data['city'] = $_POST['city'];
            $data['area'] = empty($_POST['area']) ? NULL : $_POST['area'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '系统设置';
            $data_log['type'] = '系统设置';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '编辑组织：' . $data['name'];

            $edit = OrganizationModel::editOrganization($data);
            OperationModel::addOperation($data_log);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 删除组织
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $data_redis = $this->request->middleware('data_redis');

        $id = $_POST['id'];
        $list = OrganizationModel::getOrganization($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '系统设置';
        $data_log['type'] = '系统设置';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $this->request->pathinfo();
        $data_log['log'] = '删除组织：' . $list['name'];

        $del = OrganizationModel::delOrganization($id);
        OperationModel::addOperation($data_log);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * see 查看组织详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $list = OrganizationModel::getOrganization($id);

        $province = OrganizationModel::getCity($list['province']);
        if ($province) {
            $list['province'] = $province['name'];
        }

        $city = OrganizationModel::getCity($list['city']);
        if ($city) {
            $list['city'] = $city['name'];
        }

        $area = OrganizationModel::getCity($list['area']);
        if ($area) {
            $list['area'] = $area['name'];
        }

        $p_organization = OrganizationModel::getOrganization($list['pid']);
        if ($p_organization) {
            $list['p_organization'] = $p_organization['name'];
        }

        $industry = OrganizationModel::getIndustry($list['industry_id']);
        if ($industry) {
            $list['industry'] = $industry['name'];
        }

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * find 组织回显
     * 
     * @author wuyinghua
	 * @return void
     */
    public function find() {
        $data_redis = $this->request->middleware('data_redis');
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $id = $_GET['id'];
        $list = OrganizationModel::getOrganization($id);
        $type = $list['type'];
        $list['type'] = [];
        $list['type']['id'] = OrganizationModel::TYPE_BRANCH_COMPANY;
        $list['type']['name'] = $type;

        $province = OrganizationModel::getCity($list['province']);
        if ($province) {
            $list['province'] = [];
            $list['province']['id'] = $province['id'];
            $list['province']['name'] = $province['name'];
        }

        $city = OrganizationModel::getCity($list['city']);
        if ($city) {
            $list['city'] = [];
            $list['city']['id'] = $city['id'];
            $list['city']['name'] = $city['name'];
        }

        $area = OrganizationModel::getCity($list['area']);
        if ($area) {
            $list['area'] = [];
            $list['area']['id'] = $area['id'];
            $list['area']['name'] = $area['name'];
        }

        $p_organization = OrganizationModel::getOrganization($list['pid']);
        if ($p_organization) {
            $list['p_organization'] = [];
            $list['p_organization']['id'] = $p_organization['id'];
            $list['p_organization']['name'] = $p_organization['name'];
        }

        $industry = OrganizationModel::getIndustry($list['industry_id']);
        if ($industry) {
            $list['industry'] = [];
            $list['industry']['id'] = $industry['id'];
            $list['industry']['name'] = $industry['name'];
        }

        $list_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();

        // 查询当前组织的母公司
        $main_organization_id = self::getParent($id);

        // 上级组织不包括第三级 只显示所属母公司
        $temp_arr = [];
        foreach ($list_all as $key => $value) {
            $organization = OrganizationModel::getOrganization($value['pid']);

            if ($value['pid'] == 0 || $organization['pid'] == 0 && $value['id'] != $id) {
                $temp_arr[$key]['id'] = $value['id'];
                $temp_arr[$key]['name'] = $value['name'];
            }
        }

        $superior_organization = [];
        foreach ($temp_arr as $key => $value) {
            $organization = OrganizationModel::getOrganization($value['id']);

            if ($organization['pid'] == $main_organization_id || $value['id'] == $main_organization_id) {
                $superior_organization[$key] = $value;
            }

        }

        $data['code'] = 200;
        $data['data'] = $list;
        $data['data']['superior_organization'] = array_values($superior_organization);

        return json($data);
    }

    /**
     * getTree 获取父子集
     * 
     * @author wuyinghua
     * @param $data
     * @param $pid
     * @param $level
	 * @return $data
     */
    public static function getTree($data, $pid, $level) {
        $tree = [];
        foreach($data as $k => $v) {
            $level_value = isset($v['level']) ? $v['level'] : '';
            if ($level_value == $level) {
                break;
            }
            if($v['pid'] == $pid) {
                $v['children'] = self::getTree($data, $v['id'], $level);
                $tree[] = $v;
                unset($data[$k]);
            }
        }

        return $tree;
    }

    /**
     * getParent 获取父级
     * 
     * @author wuyinghua
     * @param $datas
     * @param $id
	 * @return $parent_id
     */
    public static function getParent($id) {
        $parent_id = '';
        $organization = OrganizationModel::getOrganization($id);

        if ($organization['pid'] == 0) {
            $parent_id = $organization['id'];
        } else {
            $parent_id = self::getParent($organization['pid']);
        }
        return $parent_id;
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            if (empty($data['pid'])) {
                validate(OrganizationParentValidate::class)->check($data);
            } else {
                validate(OrganizationValidate::class)->check($data);
            }

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}