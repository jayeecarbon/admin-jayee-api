<?php
declare (strict_types = 1);

namespace app\controller\product;

use app\BaseController;
use app\model\product\FactorModel;

class Factor extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 排放因子列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：排放因子名称、发布国家/组织、发布年份
        $filters = [
            'filter_factor_source' => isset($_GET['filterFactorSource']) ? $_GET['filterFactorSource'] : '',
            'filter_factor_name' => isset($_GET['filterFactorName']) ? $_GET['filterFactorName'] : '',
            'filter_country' => isset($_GET['filterCountry']) ? $_GET['filterCountry'] : '',
            'filter_year' => isset($_GET['filterYear']) ? $_GET['filterYear'] : ''
        ];

        $db = new FactorModel();
        $list = $db->getFactors($page_size, $page_index, $filters)->toArray();
        $countrys = $db->getCountrys();
        $years = $db->getYears();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['countrys'] = $countrys;
        $data['data']['years'] = $years;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * see 查看排放因子详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new FactorModel();
        $list = $db->seeFactor($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * shotSee 查看排放因子快照详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function shotSee() {
        $id = $_GET['id'];

        $db = new FactorModel();
        $list = $db->seeShotFactor($id);
        if ($list == null) {
            return json(['code'=>201, 'message'=>"未查询到因子相关信息"]);
        }

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }
}
