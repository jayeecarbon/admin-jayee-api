<?php
declare (strict_types = 1);

namespace app\middleware;

use think\cache\driver\Redis;
use think\Exception;
use think\facade\Config;

class CheckToken
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */

    public function handle($request, \Closure $next)
    {
        try {
            $token = $request->header('token');
            if($token != 'null' && $token != null) {
                $redis = new Redis(Config::get('cache.stores.redis'));
                $data_redis = $redis->get($token);

                if ($data_redis) {
                    $request->withMiddleware(['data_redis' => $data_redis]);
                } else {
                    throw new Exception('token过期', 201);
                }
            } else {
                throw new Exception('token失效', 201);
            }
        } catch (Exception $err){
            return json(['code'=>$err->getCode(), 'message'=>$err->getMessage()]);
        }

        return $next($request);
    }
}
